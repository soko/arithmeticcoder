/* ArithmeticCoder.h
 *
 *   by Lazar Sumar, 12/01/2014 (dd/mm/yyyy)
 *
 * All rights reserved.
 *
 */
#ifndef ARITHMETIC_CODER_H
#define ARITHMETIC_CODER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void ResetEncoder();
void FlushByteBuffer(uint8_t* outBuffer, size_t* outBufferIndex);
void EncodeSymbol(uint8_t symbol, uint16_t* cumProbTable, uint16_t cumProbTotal, uint8_t* outBuffer, size_t* outBufferIndex);
int DecodeSymbol(uint8_t* inBuffer, uint16_t* probabilityTable, uint8_t* outSymbol);

#ifdef __cplusplus
};
#endif

#endif /* ARITHMETIC_CODER_H */
