/* BitArray.h
 *
 *   by Lazar Sumar, 24/03/2014 (dd/mm/yyyy)
 *
 * All rights reserved. Inspired by Michael Dipperstein. See his version on http://michael.dipperstein.com/arithmetic/
 *
 */
#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

void BitArraySet(void* bitArray, size_t bitIndex, int value);
int BitArrayGet(void* bitArray, size_t bitIndex);

#ifdef __cplusplus
};
#endif

#endif /* BIT_ARRAY_H */
