/* BitArray.c
 *
 *   by Lazar Sumar, 24/03/2014 (dd/mm/yyyy)
 *
 * All rights reserved. Inspired by Michael Dipperstein. See his version on http://michael.dipperstein.com/arithmetic/
 *
 */
#include "BitArray.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

const uint8_t cHighestBitMask = 1 << ((sizeof(uint8_t) * 8) - 1);

void BitArraySet(void* bitArray, size_t bitIndex, int value)
{
    uint8_t* byteArray = (uint8_t*)bitArray;

    if (value)
    {
        byteArray[bitIndex/8] |= cHighestBitMask >> (bitIndex % 8);
    } else
    {
        byteArray[bitIndex/8] &= ~(cHighestBitMask >> (bitIndex % 8));
    }
}

int BitArrayGet(void* bitArray, size_t bitIndex)
{
    uint8_t* byteArray = (uint8_t*)bitArray;

    return !!( byteArray[bitIndex/8] & (cHighestBitMask >> (bitIndex % 8)) );
}

#ifdef __cplusplus
};
#endif
