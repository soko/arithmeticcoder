/* ArithmeticCoder.c
 *
 *   by Lazar Sumar, 12/01/2014 (dd/mm/yyyy)
 *
 * All rights reserved.
 *
 * This Arithmetic Coding implementation is a work in progress. As it stands
 * it is a stateful implementation that can not perform more than one
 * encoding/decoding operation at once.
 */
#include "ArithmeticCoder.h"
#include "BitArray.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PRESCICE_STATIC_MODEL
#define DEBUG

#ifdef DEBUG
#define DBG(x) x
#else
#define DBG(x)
#endif

#define MSB_MASK(type) (1 << (sizeof(type) * 8 - 1))
#define MSB MSB_MASK(uint16_t)

/* A symbol range must be continuous from 0 to NumberOfSymbols */

/* gHighBound - represents the current symbol range's upper bound.
 * gLowBound  - represents the current symbol range's lower bound.
 */
uint16_t gHighBound, gLowBound;

/* gHighRem - Is the remainder of the integer division performed in the last
 *            computation of the gHighBound.
 * gLowRem  - Is the remainder of the integer division performed in the last
 *            computation of the gLowBound.
 */
uint16_t gHighRem, gLowRem;
uint16_t gTotal;
size_t gUnderflowCount;

/* function: ResetEncoder()
 *   This function resets the state of the encoder thereby initialising it
 *   for a new decoding/encoding operation.
 */
void ResetEncoder()
{
    gUnderflowCount = 0;
    gHighBound = ~0; // Set to all 1's
    gLowBound = 0;   // Set to all 0's
    gHighRem = 0;    // Set to 0
    gLowRem = 0;     // Set to 0

    DBG(printf("Encoder reset:\n"));
    DBG(printf("  high: %08x\n", gHighBound));
    DBG(printf("  low:  %08x\n", gLowBound));
    DBG(printf("  MSB:  %08x\n", MSB));
}

static void CheckAndOutputBits(uint8_t* outBuffer, size_t* outBufferIndex)
{
    int shouldTryAgain;

    do {
        shouldTryAgain = 0;

        // Check if the upper and lower bounds lie in the same half of the range.
        if ((gHighBound & MSB) == (gLowBound & MSB))
        {
            // TODO: Output the underflow then the bit!!!
            while (gUnderflowCount--)
            {
                DBG(printf("%d", (gHighBound & MSB)?3:2));
                BitArraySet( outBuffer, *outBufferIndex, (gHighBound & MSB) );
                ++(*outBufferIndex);
            }
            DBG(printf("%d", (gHighBound & MSB)?1:0));
            BitArraySet( outBuffer, *outBufferIndex, (gHighBound & MSB) );
            ++(*outBufferIndex);
            gUnderflowCount = 0;

            // Shift the bits out.
            gHighBound <<= 1;
            gHighBound += 1;
            gLowBound <<= 1;
            
#ifdef PRESCICE_STATIC_MODEL
            gHighRem <<= 1;
            gLowRem <<= 1;
            
            gHighBound += gHighRem / gTotal;
            gHighRem %= gTotal;
            gLowBound += gLowRem / gTotal;
            gLowRem %= gTotal;
#endif

            DBG(printf("\nc: high: %08x, low: %08x, gUnderflow: %ld\n", gHighBound, gLowBound, gUnderflowCount)); 
            
            shouldTryAgain = 1;
        } else if ((gHighBound & (MSB >> 1)) < (gLowBound & (MSB >> 1)))
        {
            // TODO: Record underflow!!!
            gUnderflowCount++;

            // Shift out the underflow bit.
            gHighBound = ((gHighBound << 1) & ~MSB) | (gHighBound & MSB);
            gHighBound += 1;
            gLowBound = ((gLowBound << 1) & ~MSB) | (gLowBound & MSB);
            
#ifdef PRESCICE_STATIC_MODEL
            gHighRem <<= 1;
            gLowRem <<= 1;
            
            gHighBound += gHighRem / gTotal;
            gHighRem %= gTotal;
            gLowBound += gLowRem / gTotal;
            gLowRem %= gTotal;
#endif
            
            DBG(printf("\nd: high: %08x, low: %08x, gUnderflow: %ld\n", gHighBound, gLowBound, gUnderflowCount));
            
            shouldTryAgain = 1;
        } else if ((gHighBound & MSB) == 0 && (gLowBound & MSB) != 0)
        {
            // TODO: Record error!!!
            DBG(fprintf(stderr, "\nBounds error! high: %04x, low: %04x\n", gHighBound, gLowBound));
        }
    } while (shouldTryAgain);
    printf("\n");
}

// TODO: Check maxSymbolBounds limitation to 15 bits!!!
static void ApplySymbolRange(uint16_t symbolLowBound,
                             uint16_t symbolHighBound,
                             uint16_t maxSymbolBound)
{
    // Compute the current range (inclusive)
    uint32_t range = (uint32_t)(gHighBound - gLowBound) + 1;
    uint32_t high = gHighBound;
    uint32_t low = gLowBound;
    
    gTotal = maxSymbolBound;
    
    DBG(printf("b: high: %08x, low: %08x, range: %d, hrem: %d, lrem: %d\n", high, low, range, gHighRem, gLowRem));
    
    // Compute the new bounds
    high = low + (range * symbolHighBound + gHighRem) / maxSymbolBound - 1;
    low = low + (range * symbolLowBound + gLowRem) / maxSymbolBound;

    gHighBound = high;
    gLowBound = low;

#ifdef PRESCICE_STATIC_MODEL
    /* Compute the remainders of the integer divisions performed that will be
     * carried through to the next iteration.
     */
    gHighRem = (gHighRem % maxSymbolBound) + (range * symbolHighBound) % maxSymbolBound;
    gLowRem = (gLowRem % maxSymbolBound) + (range * symbolLowBound) % maxSymbolBound;
#endif

    DBG(printf("a: high: %08x, low: %08x, range: %d, hrem: %d, lrem: %d\n", high, low, range, gHighRem, gLowRem));
}

void EncodeSymbol(uint8_t symbol, uint16_t* cumProbTable, uint16_t cumProbTotal, uint8_t* outBuffer, size_t* outBufferIndex)
{
    uint16_t highCount, lowCount;

    // Get the cumulative probability counts for the symbols.
    highCount = cumProbTable[symbol + 1];
    lowCount = cumProbTable[symbol];

    // Perform the range reduction step.
    ApplySymbolRange(lowCount, highCount, cumProbTotal);

    // Do the scaling tests.
    CheckAndOutputBits(outBuffer, outBufferIndex);
}

int DecodeSymbol(uint8_t* inBuffer, uint16_t* cumProbTable, uint8_t* outSymbol)
{
    // TODO: Implement!!!
    return 0;
}

#ifdef __cplusplus
};
#endif
