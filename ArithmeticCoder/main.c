/* main.c
 *
 *   by Lazar Sumar, 12/01/2014 (dd/mm/yyyy)
 *
 * All rights reserved.
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ArithmeticCoder.h"
#include "BitArray.h"

#define ARRAY_LEN(array) (sizeof(array)/sizeof(array[0]))

uint8_t* GenerateRandomSymbolList(uint16_t numberOfSymbols, size_t listSize)
{
    size_t i;
    uint8_t* rv = malloc(listSize * sizeof(*rv));

    memset(rv, 0, sizeof(*rv) * listSize);

    for (i = 0; i < listSize; ++i)
    {
        rv[i] = rand() % numberOfSymbols;
    }

    return rv;
}

uint16_t* GenerateSymbolProbabilityList(uint8_t* symbolList, uint16_t numberOfSymbols, size_t listSize)
{
    size_t i;
    uint16_t* rv = malloc(numberOfSymbols * sizeof(*rv));

    memset(rv, 0, numberOfSymbols * sizeof(*rv));

    for (i = 0; i < listSize; ++i)
    {
        if (symbolList[i] % numberOfSymbols == symbolList[i])
        {
            rv[symbolList[i]]++;
        } else
        {
            printf("Symbol Probability Generation Error!\n");
        }
    }

    return rv;
}

uint16_t* GenerateCumProbabilityList(uint16_t* symbolProbList, uint16_t numberOfSymbols)
{
    uint16_t i;
    uint16_t* rv = malloc((numberOfSymbols + 1) * sizeof(*rv));

    rv[0] = 0;
    for (i = 0; i < numberOfSymbols; ++i)
    {
        rv[i + 1] = rv[i] + symbolProbList[i];
    }

    return rv;
}

void PrintByteBuffer(uint8_t* buffer, size_t size)
{
    size_t i;

    for (i = 0; i < size; ++i)
    {
        printf("%c",'0' + BitArrayGet(buffer, i));
        
        if( i && i % 8 == 0 )
            printf(" ");
    }

    printf("\n");
}

size_t ComputeTotal(uint16_t* symbolProbList, size_t numberOfSymbols)
{
    size_t i;
    size_t total = 0;

    for (i = 0; i < numberOfSymbols; ++i)
    {
        total += symbolProbList[i];
    }

    return total;
}

int main()
{
    const int numberOfSymbols = 4;
    const int symbolListSize = 20;
    
    uint8_t* symbolList = GenerateRandomSymbolList(numberOfSymbols, symbolListSize);
    uint16_t* probList = GenerateSymbolProbabilityList(symbolList, numberOfSymbols, symbolListSize);
    uint16_t* cumProb = GenerateCumProbabilityList(probList, numberOfSymbols);
    
    uint16_t total = ComputeTotal(probList, numberOfSymbols);
    
    int i;
    
    uint8_t outBuffer[10000];
    size_t outBufferIndex = 0;

    printf("total: %d\n", total);
    printf("Symbol probabilities [symbol, probability, cumulative probability]:\n");
    for (i = 0; i < numberOfSymbols; ++i)
    {
        printf("  [%d, %2d, %2d]\n", i, probList[i], cumProb[i]);
    }

    printf("Encoding symbols: \n");
    for (i = 0; i < symbolListSize; ++i)
    {
        if (i % 10)
        {
            printf(", ");
        } else
        {
            printf("\n");
        }
        printf("%d", symbolList[i]);
    }
    printf("\n");

    printf("\n");

    ResetEncoder();
    for (i = 0; i < total; ++i)
    {
        EncodeSymbol(symbolList[i], cumProb, total, outBuffer, &outBufferIndex);
    }

    printf("size: %lu\n", outBufferIndex);
    PrintByteBuffer(outBuffer, outBufferIndex);

    printf("\n");

    free(symbolList);
    free(probList);
    free(cumProb);

    return 0;
}

