CC=clang
OUTDIR=ArithmeticCoder/Debug/
INDIR=ArithmeticCoder/

$(OUTDIR)ArithmeticCoder: $(INDIR)main.c $(OUTDIR)ArithmeticCoder.o $(OUTDIR)BitArray.o
	$(CC) -o $(OUTDIR)ArithmeticCoder $(INDIR)main.c $(OUTDIR)ArithmeticCoder.o $(OUTDIR)BitArray.o

$(OUTDIR)ArithmeticCoder.o: $(INDIR)ArithmeticCoder.c $(INDIR)ArithmeticCoder.h $(OUTDIR)BitArray.o
	$(CC) -c $(INDIR)ArithmeticCoder.c -o $(OUTDIR)ArithmeticCoder.o

$(OUTDIR)BitArray.o: $(INDIR)BitArray.c $(INDIR)BitArray.h
	$(CC) -c $(INDIR)BitArray.c -o $(OUTDIR)BitArray.o

clean:
	rm -R $(OUTDIR)*
